﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shakers.Api.Business.Shakers.Api.Business.Model.Enum
{
    public class AlcoholEnumDTO
    {
        public int AlcoholEnumId { get; set; }
        public string AlcoholName { get; set; } 
    }
}
