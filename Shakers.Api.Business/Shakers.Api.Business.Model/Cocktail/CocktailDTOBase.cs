﻿using Shakers.Api.Business.Shakers.Api.Business.Model.Enum;
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Business
{
    public class CocktailDTOBase
    {
        public int CocktailId { get; set; }
        public string CocktailName { get; set; }
        
        public string CocktailUrlImage { get; set; }
        public int CocktailNbApparition { get; set; }

        public int AlcoholEnumId { get; set; }
        public int TasteEnumId { get; set; }


        public ICollection<LignIngredientDTOBaseForCocktail> LignIngredientDTOBaseForCocktail { get; set; }


    }
}
