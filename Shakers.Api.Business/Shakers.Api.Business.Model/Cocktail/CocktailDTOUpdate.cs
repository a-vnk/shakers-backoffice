﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shakers.Api.Business
{
   public class CocktailDTOUpdate
    {
        public string CocktailName { get; set; }
        public string CocktailUrlImage { get; set; }
        public int AlcoholEnumId { get; set; }
        public int TasteEnumId { get; set; }
    }
}
