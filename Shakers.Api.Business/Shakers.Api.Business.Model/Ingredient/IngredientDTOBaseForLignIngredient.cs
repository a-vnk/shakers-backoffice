﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shakers.Api.Business.Shakers.Api.Business.Model.Ingredient
{
    public class IngredientDTOBaseForLignIngredient
    {
        public string IngredientName { get; set; }
    }
}
