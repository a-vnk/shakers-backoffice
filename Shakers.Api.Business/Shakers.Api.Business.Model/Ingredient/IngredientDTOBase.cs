﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shakers.Api.Business
{
    public class IngredientDTOBase
    {
        public string IngredientName { get; set; }

        public int CategoryEnumId { get; set; }
    }
}
