﻿using Shakers.Api.Business.Shakers.Api.Business.Model.Enum;
using Shakers.Api.Business.Shakers.Api.Business.Model.Ingredient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shakers.Api.Business
{
    public class LignIngredientDTOBaseForCocktail
    {
        public IngredientDTOBaseForLignIngredient Ingredient { get; set; }

        public string? RecipeQuantity { get; set; }
        public UnityEnumDTO UnityEnum { get; set; }
    }
}
