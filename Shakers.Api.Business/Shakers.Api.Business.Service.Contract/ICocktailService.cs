﻿



using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Business
{
    public interface ICocktailService
    {
        Task<ICollection<CocktailDTOBase>> GetAllCocktailsAsync();

        Task<CocktailDTOBase> GetCocktailByIdAsync(int cocktailId);

        Task<CocktailDTOBase> CocktailCreateAsync(CocktailDTOCreate newCocktail);

        Task<CocktailDTOBase> CocktailUpdateAsync(int cocktailId, CocktailDTOUpdate updateCocktail);

        Task<Cocktail> CocktailDeleteAsync(int cocktailId);

    }

}
