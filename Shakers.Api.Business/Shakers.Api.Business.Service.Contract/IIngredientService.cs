﻿
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Business
{
    public interface IIngredientService
    {
        Task<ICollection<IngredientDTOBase>> GetAllIngredientsAsync();

        Task<ICollection<IngredientDTOBase>> GetAllIngredientsByCategoryAsync(int categoryId);

        Task<IngredientDTOBase> GetIngredientByIdAsync(int ingedientId);

        Task<IngredientDTOBase> IngredientCreateAsync(IngredientDTOBase newIngredient);

        Task<Ingredient> IngredientDeleteAsync(int ingredientId);
    }
}
