﻿using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;
using System.Collections.ObjectModel;

namespace Shakers.Api.Business
{
    public class IngredientService : IIngredientService
    {
        private readonly IIngredientRepository _ingredientRepository;

        private readonly MapperModel _mapper;

        public IngredientService(IIngredientRepository ingredientRepository, MapperModel mapper)
        {
            _ingredientRepository = ingredientRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<IngredientDTOBase>> GetAllIngredientsAsync()
        {
            var ingredients = await _ingredientRepository.GetAllAsync();
            var ingredientDTOs = new Collection<IngredientDTOBase>();

            foreach (var ingredient in ingredients)
            {
                var ingredientDTO = _mapper.MapIngredientDTOBase(ingredient);
                ingredientDTOs.Add(ingredientDTO);
            }

            return ingredientDTOs;
        }

        public async Task<ICollection<IngredientDTOBase>> GetAllIngredientsByCategoryAsync(int categoryId)
        {
            var ingredients = await _ingredientRepository.GetIngredientByCategoryAsync(categoryId);
            var ingredientDTOs = new Collection<IngredientDTOBase>();

            foreach (var ingredient in ingredients)
            {
                var ingredientDTO = _mapper.MapIngredientDTOBase(ingredient);
                ingredientDTOs.Add(ingredientDTO);
            }

            return ingredientDTOs;
        }

        public async Task<IngredientDTOBase> GetIngredientByIdAsync(int ingedientId)
        {
            var ingredient = await _ingredientRepository.GetByKeyAsync(ingedientId);
            var ingredientDTO = _mapper.MapIngredientDTOBase(ingredient);

            return ingredientDTO;
        }

        public async Task<IngredientDTOBase> IngredientCreateAsync(IngredientDTOBase newIngredient)
        {
            var ingredient = _mapper.MapIngredientDTOCreateToEntity(newIngredient);
            var createdIngredient = await _ingredientRepository.CreateElementAsync(ingredient);

            return _mapper.MapIngredientDTOBase(createdIngredient);
        }

        public async Task<Ingredient> IngredientDeleteAsync(int ingredientId)
        {
            var ingredient = await _ingredientRepository.GetByKeyAsync(ingredientId);
            var deletedIngredient = await _ingredientRepository.DeleteElementAsync(ingredient);

            return deletedIngredient;
        }
    }
}
