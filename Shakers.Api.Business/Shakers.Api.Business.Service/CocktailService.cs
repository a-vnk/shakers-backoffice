﻿using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;
using System.Collections.ObjectModel;

namespace Shakers.Api.Business
{
    public class CocktailService : ICocktailService
    {
        private readonly ICocktailRepository _cocktailRepository;

        private readonly MapperModel _mapper;

        public CocktailService(ICocktailRepository cocktailRepository, MapperModel mapper)
        {
            _cocktailRepository = cocktailRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<CocktailDTOBase>> GetAllCocktailsAsync()
        {
            var cocktails = await _cocktailRepository.GetCocktailsAsync();
            var cocktailDTOs = new Collection<CocktailDTOBase>();

            foreach (var cocktail in cocktails)
            {
                var cocktailDTO = _mapper.MapCocktailToDTO(cocktail);
                cocktailDTOs.Add(cocktailDTO);
            }


            return cocktailDTOs;
        }

        public async Task<CocktailDTOBase> GetCocktailByIdAsync(int id)
        {
            var cocktail = await _cocktailRepository.GetCocktailByIdAsync(id);
            var cocktailDTO = _mapper.MapCocktailToDTO(cocktail);

            return cocktailDTO;
        }

        public async Task<CocktailDTOBase> CocktailCreateAsync(CocktailDTOCreate newCocktail)
        {
            var cocktail = _mapper.MapCocktailDTOCreateToEntity(newCocktail);
            var createdCocktail = await _cocktailRepository.CreateElementAsync(cocktail);

            return _mapper.MapCocktailToDTO(createdCocktail);
        }

        

        public async Task<CocktailDTOBase> CocktailUpdateAsync(int cocktailId, CocktailDTOUpdate updateCocktail)
        {
            var cocktail = _mapper.MapCocktailDTOUpdateToEntity(cocktailId, updateCocktail);
            var updatedCocktail = await _cocktailRepository.UpdateElementAsync(cocktail);

            return _mapper.MapCocktailToDTO(updatedCocktail);
        }

        public async Task<Cocktail> CocktailDeleteAsync(int cocktailId)
        {
            var cocktail = await _cocktailRepository.GetCocktailByIdAsync(cocktailId);

            return await _cocktailRepository.DeleteElementAsync(cocktail);

           
        }



    }
}
