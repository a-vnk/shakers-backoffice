﻿using AutoMapper;
using Shakers.Api.Business.Shakers.Api.Business.Model.Enum;
using Shakers.Api.Business.Shakers.Api.Business.Model.Ingredient;
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Business
{
    public class MapperModel
    {
        public CocktailDTOBase MapCocktailToDTO(Cocktail cocktail)
        {
            var cocktailDTO = new CocktailDTOBase
            {
                CocktailId = cocktail.CocktailId,
                CocktailName = cocktail.CocktailName,
                CocktailUrlImage = cocktail.CocktailUrlImage,
                CocktailNbApparition = cocktail.CocktailNbApparition,
                AlcoholEnumId = cocktail.AlcoholEnumId,
                TasteEnumId = cocktail.TasteEnumId,
                LignIngredientDTOBaseForCocktail = new List<LignIngredientDTOBaseForCocktail>()
            };

            foreach (var lignIngredient in cocktail.LignIngredients)
            {
                var ingredientDTO = new IngredientDTOBaseForLignIngredient
                {
                    IngredientName = lignIngredient.Ingredient.IngredientName
                };

                var unityEnumDTO = lignIngredient.UnityEnum != null ? new UnityEnumDTO
                {
                    UnityName = lignIngredient.UnityEnum.UnityName
                } : null;

                var lignIngredientDTO = new LignIngredientDTOBaseForCocktail
                {
                    Ingredient = ingredientDTO,
                    RecipeQuantity = lignIngredient.RecipeQuantity,
                    UnityEnum = unityEnumDTO

                };

                

                cocktailDTO.LignIngredientDTOBaseForCocktail.Add(lignIngredientDTO);
            }

            return cocktailDTO;
        }

        public Cocktail MapCocktailDTOCreateToEntity(CocktailDTOCreate cocktail)
        {
            var cocktailEntity = new Cocktail
            {
               CocktailName = cocktail.CocktailName,
                CocktailUrlImage = cocktail.CocktailUrlImage,
                AlcoholEnumId = cocktail.AlcoholEnumId,
                TasteEnumId = cocktail.TasteEnumId
            };

            return cocktailEntity;
        }

        public Cocktail MapCocktailDTOUpdateToEntity(int cocktailId, CocktailDTOUpdate updateCocktail)
        {
            
            var cocktailEntity = new Cocktail
            {
                CocktailId = cocktailId,
                CocktailName = updateCocktail.CocktailName,
                CocktailUrlImage = updateCocktail.CocktailUrlImage,
                AlcoholEnumId = updateCocktail.AlcoholEnumId,
                TasteEnumId = updateCocktail.TasteEnumId
            };

            return cocktailEntity;

            
        }

        public IngredientDTOBase MapIngredientDTOBase(Ingredient ingredient)
        {
            var ingredientDTO = new IngredientDTOBase
            {
                IngredientName = ingredient.IngredientName,
                CategoryEnumId = ingredient.CategoryEnumId
            };

           

            return ingredientDTO;
        }

        public Ingredient MapIngredientDTOCreateToEntity(IngredientDTOBase ingredient)
        {
            var ingredientEntity = new Ingredient
            {
                IngredientName = ingredient.IngredientName,
                CategoryEnumId = ingredient.CategoryEnumId
            };

            return ingredientEntity;
        }

       
    }
}


