﻿
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Repository.Contract
{
    public interface ILignIngredientRepository : IGenericRepository<LignIngredient>
    {

    }
}
