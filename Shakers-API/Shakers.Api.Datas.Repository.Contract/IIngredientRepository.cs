﻿using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Repository.Contract
{
    public interface IIngredientRepository : IGenericRepository<Ingredient>
    {

        Task<ICollection<Ingredient>> GetIngredientByCategoryAsync(int categoryId);
    }
}
