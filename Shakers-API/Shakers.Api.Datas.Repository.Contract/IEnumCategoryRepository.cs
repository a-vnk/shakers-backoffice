﻿using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Repository.Contract
{
    public interface IEnumCategoryRepository : IGenericRepository<EnumCategory>
    {
    }
}
