﻿
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Repository.Contract
{
    public interface ICocktailRepository : IGenericRepository<Cocktail>
    {
        Task<ICollection<Cocktail>> GetCocktailsAsync();

        Task<Cocktail> GetCocktailByIdAsync(int cocktailId);

       
    }
}
