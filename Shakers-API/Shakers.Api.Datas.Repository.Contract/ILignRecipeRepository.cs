﻿using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Repository.Contract
{
    public interface ILignRecipeRepository : IGenericRepository<LignRecipe>
    {
    }
}
