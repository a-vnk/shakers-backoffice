﻿namespace Shakers.Api.Datas.Entities
{
    public partial class EnumUnity
    {
        public EnumUnity()
        {
            LignIngredients = new HashSet<LignIngredient>();
        }

        public int UnityEnumId { get; set; }
        public string UnityName { get; set; } = null!;

        public virtual ICollection<LignIngredient> LignIngredients { get; set; }
    }
}
