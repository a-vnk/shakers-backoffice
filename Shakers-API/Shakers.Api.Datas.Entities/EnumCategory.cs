﻿namespace Shakers.Api.Datas.Entities
{
    public partial class EnumCategory
    {
        public EnumCategory()
        {
            Ingredients = new HashSet<Ingredient>();
        }

        public int CategoryEnumId { get; set; }
        public string CategoryName { get; set; } = null!;

        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }
}
