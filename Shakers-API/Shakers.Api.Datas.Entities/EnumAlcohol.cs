﻿namespace Shakers.Api.Datas.Entities
{
    public partial class EnumAlcohol
    {
        public EnumAlcohol()
        {
            Cocktails = new HashSet<Cocktail>();
        }

        public int AlcoholEnumId { get; set; }
        public string AlcoholName { get; set; } = null!;

        public virtual ICollection<Cocktail> Cocktails { get; set; }
    }
}
