﻿namespace Shakers.Api.Datas.Entities
{
    public partial class EnumTaste
    {
        public EnumTaste()
        {
            Cocktails = new HashSet<Cocktail>();
        }

        public int TasteEnumId { get; set; }
        public string TasteName { get; set; } = null!;

        public virtual ICollection<Cocktail> Cocktails { get; set; }
    }
}
