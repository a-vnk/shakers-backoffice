﻿namespace Shakers.Api.Datas.Entities
{
    public partial class Ingredient
    {
        public Ingredient()
        {
            LignIngredients = new HashSet<LignIngredient>();
        }

        public int IngredientId { get; set; }
        public string IngredientName { get; set; } = null!;
        public int CategoryEnumId { get; set; }

        public virtual EnumCategory CategoryEnum { get; set; } = null!;
        public virtual ICollection<LignIngredient> LignIngredients { get; set; }
    }
}
