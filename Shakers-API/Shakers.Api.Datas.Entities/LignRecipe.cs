﻿namespace Shakers.Api.Datas.Entities
{
    public partial class LignRecipe
    {
        public int CocktailId { get; set; }
        public int LignRecipeEtape { get; set; }
        public string? LignDescription { get; set; }

        public virtual Cocktail Cocktail { get; set; } = null!;
    }
}
