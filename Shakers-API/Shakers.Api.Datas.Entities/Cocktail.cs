﻿namespace Shakers.Api.Datas.Entities
{
    public partial class Cocktail
    {
        public Cocktail()
        {
            LignIngredients = new HashSet<LignIngredient>();
            LignRecipes = new HashSet<LignRecipe>();
        }

        public int CocktailId { get; set; }
        public string CocktailName { get; set; } = null!;
        public int AlcoholEnumId { get; set; }
        public int TasteEnumId { get; set; }
        public DateOnly CocktailCreationDate { get; set; }
        public string CocktailUrlImage { get; set; } = null!;
        /// <summary>
        /// Nombre de fois que ce cocktail à été découvert
        /// </summary>
        public int CocktailNbApparition { get; set; }

        public virtual EnumAlcohol AlcoholEnum { get; set; } = null!;
        public virtual EnumTaste TasteEnum { get; set; } = null!;
        public virtual ICollection<LignIngredient> LignIngredients { get; set; }
        public virtual ICollection<LignRecipe> LignRecipes { get; set; }
    }
}
