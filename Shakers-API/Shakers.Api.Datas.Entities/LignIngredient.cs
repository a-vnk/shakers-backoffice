﻿namespace Shakers.Api.Datas.Entities
{
    public partial class LignIngredient
    {
        public int IngredientId { get; set; }
        public int CocktailId { get; set; }
        public string? RecipeQuantity { get; set; }
        public int? UnityEnumId { get; set; }

        public virtual Cocktail Cocktail { get; set; } = null!;
        public virtual Ingredient Ingredient { get; set; } = null!;
        public virtual EnumUnity? UnityEnum { get; set; }
    }
}
