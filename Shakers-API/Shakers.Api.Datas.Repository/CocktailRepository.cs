﻿using Microsoft.EntityFrameworkCore;
using Shakers.Api.Datas.Context;
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;
using System.Collections.Generic;

namespace Shakers.Api.Datas.Repository;

public class CocktailRepository : GenericRepository<Cocktail>, ICocktailRepository
{
    public CocktailRepository(IShakersDbContext shakersContext) : base(shakersContext)
    {
    }

    public async Task<ICollection<Cocktail>> GetCocktailsAsync()
    {
        return await _table
            .Include(c => c.LignIngredients)
                .ThenInclude(li => li.Ingredient)
             .Include(c => c.LignIngredients)
                 .ThenInclude(li => li.UnityEnum)
            .ToListAsync();
    }

    public async Task<Cocktail> GetCocktailByIdAsync(int cocktailId)
    {
        return await _table
            .Include(c => c.LignIngredients)
                .ThenInclude(li => li.Ingredient)
             .Include(c => c.LignIngredients)
                 .ThenInclude(li => li.UnityEnum)
            .FirstOrDefaultAsync(c => c.CocktailId == cocktailId);
    }

    

    



}
