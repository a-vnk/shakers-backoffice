﻿
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class LignIngredientRepository : GenericRepository<LignIngredient>, ILignIngredientRepository
    {
        public LignIngredientRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }
    }
}
