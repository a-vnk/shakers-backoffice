﻿
using Microsoft.EntityFrameworkCore;
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class IngredientRepository : GenericRepository<Ingredient>, IIngredientRepository
    {
        public IngredientRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }


        public async Task<ICollection<Ingredient>> GetIngredientByCategoryAsync(int categoryId)
        {
            return await _table
                .Where(i => i.CategoryEnumId == categoryId)
                .ToListAsync();
        }

        
    }
}
