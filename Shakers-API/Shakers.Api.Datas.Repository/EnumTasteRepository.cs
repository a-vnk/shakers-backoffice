﻿using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class EnumTasteRepository : GenericRepository<EnumTaste>, IEnumTasteRepository
    {
        public EnumTasteRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }
    }
}
