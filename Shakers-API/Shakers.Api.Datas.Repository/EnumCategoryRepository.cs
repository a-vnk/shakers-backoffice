﻿using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class EnumCategoryRepository : GenericRepository<EnumCategory>, IEnumCategoryRepository
    {
        public EnumCategoryRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }
    }
}
