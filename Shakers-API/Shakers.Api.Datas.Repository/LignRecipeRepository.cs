﻿
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class LignRecipeRepository : GenericRepository<LignRecipe>, ILignRecipeRepository
    {
        public LignRecipeRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }
    }
}
