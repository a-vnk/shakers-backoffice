﻿
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.Datas.Repository
{
    public class EnumUnityRepository : GenericRepository<EnumUnity>, IEnumUnityRepository
    {
        public EnumUnityRepository(IShakersDbContext shakersContext) : base(shakersContext)
        {
        }
    }
}
