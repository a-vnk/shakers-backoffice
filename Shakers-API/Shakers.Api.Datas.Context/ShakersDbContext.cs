﻿using Microsoft.EntityFrameworkCore;
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Context
{
    public partial class ShakersDbContext : DbContext, IShakersDbContext
    {
        public ShakersDbContext()
        {
        }

        public ShakersDbContext(DbContextOptions<ShakersDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cocktail> Cocktails { get; set; } = null!;
        public virtual DbSet<EnumAlcohol> EnumAlcohols { get; set; } = null!;
        public virtual DbSet<EnumCategory> EnumCategories { get; set; } = null!;
        public virtual DbSet<EnumTaste> EnumTastes { get; set; } = null!;
        public virtual DbSet<EnumUnity> EnumUnities { get; set; } = null!;
        public virtual DbSet<Ingredient> Ingredients { get; set; } = null!;
        public virtual DbSet<LignIngredient> LignIngredients { get; set; } = null!;
        public virtual DbSet<LignRecipe> LignRecipes { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<Cocktail>(entity =>
            {
                entity.ToTable("cocktail");

                entity.HasIndex(e => e.CocktailName, "cocktail_name")
                    .IsUnique();

                entity.HasIndex(e => e.AlcoholEnumId, "fk_alcohol_enumId");

                entity.HasIndex(e => e.TasteEnumId, "fk_taste_enumId");

                entity.Property(e => e.CocktailId).HasColumnName("cocktail_id");

                entity.Property(e => e.AlcoholEnumId).HasColumnName("alcohol_enumId");

                entity.Property(e => e.CocktailCreationDate).HasColumnName("cocktail_creationDate");

                entity.Property(e => e.CocktailName).HasColumnName("cocktail_name");

                entity.Property(e => e.CocktailNbApparition)
                    .HasColumnName("cocktail_nb_apparition")
                    .HasComment("Nombre de fois que ce cocktail à été découvert");

                entity.Property(e => e.CocktailUrlImage)
                    .HasMaxLength(255)
                    .HasColumnName("cocktail_url_image");

                entity.Property(e => e.TasteEnumId).HasColumnName("taste_enumId");

                entity.HasOne(d => d.AlcoholEnum)
                    .WithMany(p => p.Cocktails)
                    .HasForeignKey(d => d.AlcoholEnumId)
                    .HasConstraintName("fk_alcohol_enumId");

                entity.HasOne(d => d.TasteEnum)
                    .WithMany(p => p.Cocktails)
                    .HasForeignKey(d => d.TasteEnumId)
                    .HasConstraintName("fk_taste_enumId");
            });

            modelBuilder.Entity<EnumAlcohol>(entity =>
            {
                entity.HasKey(e => e.AlcoholEnumId)
                    .HasName("PRIMARY");

                entity.ToTable("enum_alcohol");

                entity.HasIndex(e => e.AlcoholName, "alcohol_name")
                    .IsUnique();

                entity.Property(e => e.AlcoholEnumId).HasColumnName("alcohol_enumId");

                entity.Property(e => e.AlcoholName).HasColumnName("alcohol_name");
            });

            modelBuilder.Entity<EnumCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryEnumId)
                    .HasName("PRIMARY");

                entity.ToTable("enum_category");

                entity.HasIndex(e => e.CategoryName, "category_name")
                    .IsUnique();

                entity.Property(e => e.CategoryEnumId).HasColumnName("category_enumId");

                entity.Property(e => e.CategoryName).HasColumnName("category_name");
            });

            modelBuilder.Entity<EnumTaste>(entity =>
            {
                entity.HasKey(e => e.TasteEnumId)
                    .HasName("PRIMARY");

                entity.ToTable("enum_taste");

                entity.HasIndex(e => e.TasteName, "taste_name")
                    .IsUnique();

                entity.Property(e => e.TasteEnumId).HasColumnName("taste_enumId");

                entity.Property(e => e.TasteName).HasColumnName("taste_name");
            });

            modelBuilder.Entity<EnumUnity>(entity =>
            {
                entity.HasKey(e => e.UnityEnumId)
                    .HasName("PRIMARY");

                entity.ToTable("enum_unity");

                entity.HasIndex(e => e.UnityName, "unity_name")
                    .IsUnique();

                entity.Property(e => e.UnityEnumId).HasColumnName("unity_enumId");

                entity.Property(e => e.UnityName).HasColumnName("unity_name");
            });

            modelBuilder.Entity<Ingredient>(entity =>
            {
                entity.ToTable("ingredient");

                entity.HasIndex(e => e.CategoryEnumId, "fk_category");

                entity.HasIndex(e => e.IngredientName, "ingredient_name")
                    .IsUnique();

                entity.Property(e => e.IngredientId).HasColumnName("ingredient_id");

                entity.Property(e => e.CategoryEnumId).HasColumnName("category_enumId");

                entity.Property(e => e.IngredientName).HasColumnName("ingredient_name");

                entity.HasOne(d => d.CategoryEnum)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.CategoryEnumId)
                    .HasConstraintName("fk_category");
            });

            modelBuilder.Entity<LignIngredient>(entity =>
            {
                entity.HasKey(e => new { e.IngredientId, e.CocktailId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("lign_ingredient");

                entity.HasIndex(e => e.CocktailId, "cocktail_id");

                entity.HasIndex(e => e.UnityEnumId, "unity_enumId");

                entity.Property(e => e.IngredientId).HasColumnName("ingredient_id");

                entity.Property(e => e.CocktailId).HasColumnName("cocktail_id");

                entity.Property(e => e.RecipeQuantity)
                    .HasMaxLength(20)
                    .HasColumnName("recipe_quantity");

                entity.Property(e => e.UnityEnumId).HasColumnName("unity_enumId");

                entity.HasOne(d => d.Cocktail)
                    .WithMany(p => p.LignIngredients)
                    .HasForeignKey(d => d.CocktailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("lign_ingredient_ibfk_2");

                entity.HasOne(d => d.Ingredient)
                    .WithMany(p => p.LignIngredients)
                    .HasForeignKey(d => d.IngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("lign_ingredient_ibfk_1");

                entity.HasOne(d => d.UnityEnum)
                    .WithMany(p => p.LignIngredients)
                    .HasForeignKey(d => d.UnityEnumId)
                    .HasConstraintName("lign_ingredient_ibfk_3");
            });

            modelBuilder.Entity<LignRecipe>(entity =>
            {
                entity.HasKey(e => new { e.CocktailId, e.LignRecipeEtape })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("lign_recipe");

                entity.Property(e => e.CocktailId).HasColumnName("cocktail_id");

                entity.Property(e => e.LignRecipeEtape).HasColumnName("lign_recipe_etape");

                entity.Property(e => e.LignDescription)
                    .HasColumnType("text")
                    .HasColumnName("lign_description");

                entity.HasOne(d => d.Cocktail)
                    .WithMany(p => p.LignRecipes)
                    .HasForeignKey(d => d.CocktailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("lign_recipe_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
