﻿using Microsoft.EntityFrameworkCore;
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Datas.Context.Contract
{
    public interface IShakersDbContext : IDbContext
    {

        DbSet<Cocktail> Cocktails { get; set; }
        DbSet<EnumAlcohol> EnumAlcohols { get; set; }
        DbSet<EnumCategory> EnumCategories { get; set; }
        DbSet<EnumTaste> EnumTastes { get; set; }
        DbSet<EnumUnity> EnumUnities { get; set; }
        DbSet<Ingredient> Ingredients { get; set; }
        DbSet<LignIngredient> LignIngredients { get; set; }
        DbSet<LignRecipe> LignRecipes { get; set; }
    }
}
