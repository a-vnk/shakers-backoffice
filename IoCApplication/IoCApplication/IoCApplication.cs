﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Shakers.Api.Business;
using Shakers.Api.Datas.Context;
using Shakers.Api.Datas.Context.Contract;
using Shakers.Api.Datas.Repository;
using Shakers.Api.Datas.Repository.Contract;

namespace Shakers.Api.IoC
{
    public static class IoCApplication
    {

        /// <summary>
        /// Configuration de l'injection des repository du Web API RestFul
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection ConfigureInjectionDependencyRepository(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Repositories

            services.AddScoped<ICocktailRepository, CocktailRepository>();
            services.AddScoped<IEnumCategoryRepository, EnumCategoryRepository>();
            services.AddScoped<IIngredientRepository, IngredientRepository>();
            services.AddScoped<ILignIngredientRepository, LignIngredientRepository>();
            services.AddScoped<ILignRecipeRepository, LignRecipeRepository>();
            services.AddScoped<IEnumTasteRepository, EnumTasteRepository>();
            services.AddScoped<IEnumUnityRepository, EnumUnityRepository>();

            return services;
        }

        public static IServiceCollection ConfigureInjectionDependencyService(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Service

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IIngredientService, IngredientService>();
            services.AddScoped<ICocktailService, CocktailService>();
            services.AddScoped<MapperModel>();

            return services;
        }


        /// <summary>
        /// Configuration de la connexion de la base de données
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("BddConnection");

            services.AddDbContext<IShakersDbContext, ShakersDbContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors());

            return services;
        }


    }
}
