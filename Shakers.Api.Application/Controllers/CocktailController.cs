﻿using Microsoft.AspNetCore.Mvc;
using Shakers.Api.Business;
using Shakers.Api.Datas.Entities;

namespace Shakers.Api.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CocktailController : ControllerBase
    {


        /// <summary>
        /// Le service de cocktail
        /// </summary>
        private readonly ICocktailService _cocktailservice;

        /// <summary>
        /// Initializes a new instance of the <see cref="CocktailController"/> class.
        /// </summary>
        /// <param name="cocktailService">The service departement.</param>
        public CocktailController(ICocktailService cocktailService)
        {
            _cocktailservice = cocktailService;
        }


        // GET: api/Cocktails
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<CocktailDTOBase>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CocktailListAsync()
        {
            var cocktails = await _cocktailservice.GetAllCocktailsAsync();

            return Ok(cocktails);
        }

        // GETLIST: api/Cocktails/{cocktailId}
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpGet("{cocktailId}")]
        [ProducesResponseType(typeof(CocktailDTOBase), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CocktailByIdAsync(int cocktailId)
        {
            var cocktails = await _cocktailservice.GetCocktailByIdAsync(cocktailId);

            return Ok(cocktails);
        }

        //POST: api/Cocktails
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(CocktailDTOBase), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CocktailCreateAsync(CocktailDTOCreate newCocktail)
        {
            var cocktails = await _cocktailservice.CocktailCreateAsync(newCocktail);


            return Ok(cocktails);

        }

        //PUT: api/Cocktails
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpPut()]
        [ProducesResponseType(typeof(CocktailDTOBase), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CocktailUpdateAsync(int cocktailId, CocktailDTOUpdate updateCocktail)
        {
            var cocktails = await _cocktailservice.CocktailUpdateAsync(cocktailId, updateCocktail);


            return Ok(cocktails);

        }

        //DELETE: api/Cocktails
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(typeof(Cocktail), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CocktailDeleteAsync(int cocktailId)
        {
            var cocktails = await _cocktailservice.CocktailDeleteAsync(cocktailId);


            return Ok();

        }

    }
}
