﻿using Microsoft.AspNetCore.Mvc;
using Shakers.Api.Business;


namespace Shakers.Api.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientController : ControllerBase
    {

        private readonly IIngredientService _ingredientService;
        public IngredientController(IIngredientService ingredientService) { 
        
            _ingredientService = ingredientService;
        }

        // GET: api/Ingredients
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<IngredientDTOBase>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> IngredientListAsync()
        {
            var ingredients = await _ingredientService.GetAllIngredientsAsync();

            return Ok(ingredients);
        }

        // GET: api/Ingredients
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpGet("filter{categoryId}")]
        [ProducesResponseType(typeof(IEnumerable<IngredientDTOBase>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> IngredientListByCategoryAsync(int categoryId)
        {
            var ingredients = await _ingredientService.GetAllIngredientsByCategoryAsync(categoryId);
            return Ok(ingredients);
        }

        // GET: api/Ingredients/{ingredientId}
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpGet("{ingredientId}")]
        [ProducesResponseType(typeof(IEnumerable<IngredientDTOBase>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> IngredienByIdAsync(int ingredientId)
        {
            var ingredients = await _ingredientService.GetIngredientByIdAsync(ingredientId);

            return Ok(ingredients);
        }

        //POST: api/Ingredients
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(CocktailDTOBase), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> IngredientCreateAsync(IngredientDTOBase newIngredient)
        {
            var ingredient = await _ingredientService.IngredientCreateAsync(newIngredient);


            return Ok(ingredient);

        }

        //DELETE: api/Ingredients
        /// <summary>
        /// Ressource pour récupérer la liste des cocktails
        /// </summary>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(typeof(CocktailDTOBase), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> IngredientDeleteAsync(int ingredientId)
        {
            var ingredient = await _ingredientService.IngredientDeleteAsync(ingredientId);


            return Ok();

        }
    }
}
