using Microsoft.Extensions.Configuration;
using Shakers.Api.Business;
using Shakers.Api.IoC;
using System.Reflection;



var builder = WebApplication.CreateBuilder(args);
IConfiguration configuration = builder.Configuration;



// Add services to the container.



builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



// Configure Database connexion
builder.Services.ConfigureDBContext(configuration);






//Dependency Injection
builder.Services.ConfigureInjectionDependencyRepository();

builder.Services.ConfigureInjectionDependencyService();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
